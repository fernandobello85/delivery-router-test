class Customer
  attr_accessor :id, :x, :y, :delivered_time

  def initialize(params)
    @id = params[:id]
    @x = params[:x]
    @y = params[:y]
    @delivered_time = 0
  end

  def current_point
    [x, y]
  end

  def update_delivered_time(time)
    @delivered_time = time
  end

  def self.find(id)
    ObjectSpace.each_object(Customer) { |c| return c if c.id == id }
  end

  def self.reset_delivered_time
    ObjectSpace.each_object(Customer) { |c| c.delivered_time = 0 }
  end
end
