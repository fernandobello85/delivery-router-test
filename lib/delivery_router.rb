require 'order'
require 'route'

class DeliveryRouter
  attr_accessor :restaurants, :customers, :riders, :orders

  def initialize(restaurants, customers, riders)
    @restaurants = restaurants
    @customers = customers
    @riders = riders
    @orders = []
    @current_routes = {}
  end

  def add_order(params)
    customer = Customer.find(params[:customer])
    restaurant = Restaurant.find(params[:restaurant])
    @orders << Order.new(customer: customer, restaurant: restaurant)
    update_routes!
  end

  def clear_orders(params = nil)
    if params.nil?
      @orders.clear
      @current_routes.clear
    end
    @orders.delete_if { |o| o.customer.id == params[:customer] }
  end

  # Every time a new order is placed, an array stores all the restaurants with orders. And every restaurant is updated with a new customer list to be delivered.
  # After, based on rider speed and location we assign a rider for every restaurant. (In a realistic context, a rider must be assigned only at the end of its first route but it is not compatible with the exercise)
  # Again over every restaurant in the list of restaurants with orders, the assigned rider is asked to find the best route to deliver all customers of the restaurant list in the minimal time.
  # As a rider can be assigned to many restaurants, a dictionary stores all the routes that allow the rider to reach all the customers of its restaurants list.

  def update_routes!
    restaurants_with_orders = []
    @current_routes.clear
    Customer.reset_delivered_time
    Restaurant.reset_customers_to_deliver

    @orders.each do |order|
      restaurant = order.restaurant
      restaurants_with_orders << restaurant.id
      restaurant.add_customer_to_deliver(order.customer)
    end
    restaurants_with_orders.uniq!
    restaurants_with_orders.each do |restaurant_id|
      restaurant = Restaurant.find(restaurant_id)
      selected_restaurant_rider = nil
      selected_restaurant_rider_time = nil
      @riders.each do |rider|
        rider_time_to_restaurant = rider.time_to_next(next_point: restaurant.current_point)
        if selected_restaurant_rider.nil? || selected_restaurant_rider_time > rider_time_to_restaurant
          selected_restaurant_rider = rider
          selected_restaurant_rider_time = rider_time_to_restaurant
        end
      end
      restaurant.update_assigned_rider(selected_restaurant_rider)
    end

    restaurants_with_orders.each do |restaurant_id|
      restaurant = Restaurant.find(restaurant_id)
      selected_restaurant_rider = restaurant.assigned_rider
      selected_restaurant_route = Route.new(rider: selected_restaurant_rider)
      rider_route, rider_delivery_time = selected_restaurant_rider.best_route_for_restaurant(restaurant)
      selected_restaurant_route.add_to_path(rider_route)
      selected_restaurant_route.add_delivery_time(rider_delivery_time)
      @current_routes[selected_restaurant_rider.id] = @current_routes[selected_restaurant_rider.id].nil? ? selected_restaurant_route.path : @current_routes[selected_restaurant_rider.id].concat(selected_restaurant_route.path)
    end
  end

  def route(params)
    @current_routes[params[:rider]] || []
  end

  def delivery_time(params)
    Customer.find(params[:customer]).delivered_time
  end
end
