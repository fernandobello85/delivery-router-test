class Order
  attr_accessor :id, :restaurant, :customer

  def initialize(customer:, restaurant:)
    @customer = customer
    @restaurant = restaurant
  end

  def self.find(id)
    ObjectSpace.each_object(Order) { |x| return x if x.id == id }
  end
end
