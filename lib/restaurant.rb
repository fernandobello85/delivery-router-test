class Restaurant
  attr_accessor :id, :x, :y, :customers_to_deliver, :cooking_time, :assigned_rider

  def initialize(params)
    @id = params[:id]
    @x = params[:x]
    @y = params[:y]
    @customers_to_deliver = []
    @cooking_time = params[:cooking_time]
    @assigned_rider = nil
  end

  def self.find(id)
    ObjectSpace.each_object(Restaurant) { |r| return r if r.id == id }
  end

  def self.reset_customers_to_deliver
    ObjectSpace.each_object(Restaurant) { |r| r.customers_to_deliver = [] }
  end

  def current_point
    [x, y]
  end

  def add_customer_to_deliver(customer)
    @customers_to_deliver.push(customer)
  end

  def update_assigned_rider(rider)
    @assigned_rider = rider
  end
end
