class Rider
  attr_accessor :id, :x, :y, :speed

  def initialize(params)
    @id = params[:id]
    @x = params[:x]
    @y = params[:y]
    @speed = params[:speed]
  end

  def update_coordinates(last_visited)
    @x = last_visited.x
    @y = last_visited.y
  end

  # Using the nearest neighborhood approach the rider can solve this NP-complete problem.
  # Staring by the restaurant the algorithm returns the path rider follow to reach every customer of the list in a minimal time.
  # At the end, the rider returns to its initial point and this time is added to the total time of rider route to keep realistic.
  def best_route_for_restaurant(restaurant)
    customers_to_visit = restaurant.customers_to_deliver.clone
    path = [restaurant]
    # Add to the time path the maximal time between the cooking time and the time the rider takes to go to the restaurant.
    path_time = [restaurant.cooking_time, time_to_next(next_point: restaurant.current_point)].max
    # Iterate until the all customers are visited
    while customers_to_visit.any?
      visited = nil
      time_to_visit = 0
      # Find the nearest customer from the current point
      customers_to_visit.each do |customer|
        time_to_customer = time_to_next(start_point: path.last.current_point, next_point: customer.current_point)
        if visited.nil? || time_to_visit > time_to_customer
          visited = customer
          time_to_visit = time_to_customer
        end
      end
      # Add the visited customers to the rider route, and saves the deliver time for every customer
      path << visited
      path_time += time_to_visit
      customers_to_visit.delete(visited)
      visited.update_delivered_time(path_time)
    end
    # Add to total path time the time to return to the initial point
    path_time += time_to_next(start_point: path.last.current_point, next_point: [x, y])
    [path, path_time]
  end

  # To identify the best path, the raider measure the euclidean distance between two points with a weight (in this case the speed).
  # The result is the tima (in minutes) that the rider takes to go from one point to another point.
  def time_to_next(start_point: nil, next_point:)
    start_point = [x, y] if start_point.nil?
    60 / speed * euclidean_distance([start_point[0], start_point[1]], [next_point[0], next_point[1]])
  end

  def euclidean_distance(point1, point2)
    Math.sqrt(point1.zip(point2).reduce(0) { |sum, p| sum + (p[0] - p[1]) ** 2 })
  end
end
