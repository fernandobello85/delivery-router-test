class Route
  attr_accessor :rider, :delivery_time, :path

  def initialize(rider:)
    @rider = rider
    @delivery_time = 0
    @path = []
  end

  def add_delivery_time(time)
    @delivery_time += time
  end

  def add_to_path(element)
    element = Array(element) unless element.is_a?(Array)
    (@path << element).flatten!
  end
end
